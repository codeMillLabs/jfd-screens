// @flow

import * as React from "react";

import { Site } from "tabler-react";

type Props = {|
  +children: React.Node,
|};

type subNavItem = {|
  +value: string,
  +to ?: string,
  +icon ?: string,
  +LinkComponent ?: React.ElementType,
|};

type navItem = {|
  +value: string,
  +to ?: string,
  +icon ?: string,
  +active ?: boolean,
  +LinkComponent ?: React.ElementType,
  +subItems ?: Array < subNavItem >,
|};






class SiteWrapper extends React.Component<Props, void> {
  render(): React.Node {

    const image = (this.props.headerImage) ? this.props.headerImage : '/demo/brand/jfd-vip-logo.JPG';

    return (
      <Site.Wrapper
        headerProps={{
          alt: "Jayaratne Funeral Directors",
          imageURL: image,
        }}
        footerProps={{
          copyright: (
            <React.Fragment>
              Copyright © 2019 Qualito.net, All rights reserved.
            </React.Fragment>
          )
        }}
      >
        {this.props.children}
      </Site.Wrapper>
    );
  }
}

export default SiteWrapper;
