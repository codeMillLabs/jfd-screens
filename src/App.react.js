import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import CustomerDataFormPage from "./customerData/CustomerDataFormPage.react";
import CustomerDashboardViewPage from "./customerData/CustomerDashboardViewPage.react";
import CustomerDataViewPage from "./customerData/CustomerDataViewPage.react";

import ObituaryNoteForm from "./obituaryNote/ObituaryNoteFormPage.react";
import ObituaryNote from "./obituaryNote/ObituaryNote.react";
import ParlourSummary from "./obituaryNote/ParlourSummary.react";

import "tabler-react/dist/Tabler.css";
import "./App.css";

type Props = {||};

function App(props: Props): React.Node {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        
        <Route exact path="/dashboard" component={CustomerDashboardViewPage} />
        <Route exact path="/customer" component={CustomerDataFormPage} />
        <Route exact path="/customer/search" component={CustomerDataViewPage} />
        <Route exact path="/customer/:cusId" component={CustomerDataFormPage} />

        <Route exact path="/obituaryNote/:location/:parlourId" component={ObituaryNote} />
        <Route exact path="/parlour/:location" component={ParlourSummary} />

        <Route exact path="/" component={ObituaryNoteForm} />
      </Switch>
    </Router>
  );
}

export default App;
