import React, { Component }from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import {
    Page,
    Grid,
    Form,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";
import 'react-table/react-table.css'


class CustomerDataViewPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchText: '',
            iFrameHeight: '1200px'

        }
    
        this.searchData = this.searchData.bind(this);
        this.onLoad = this.onLoad.bind(this);
    }

    onLoad = () => {
        const value = {
            memberId: '',
            customerName: '',
            deceasedName: '',
            telNumber: '',
            email: '',
            address1: '',
            address2: '',
            city: '',
            svc_jfd: '',
            svc_vip: '',
            svc_parlour: '',
            desc_1: '',
            formErrors: {}
        };
        this.setState(value);
    }


    searchData = ({ field, value }) => {

        if (value.lenght < 3) {
            return false;
        }

        this.setState({ [field]: value });

        const searchQuery = {
            searchText: this.state.searchText
        };

        console.log(searchQuery);


        axios.post(`/api/customer/search`, searchQuery)
            .then(function (response) {

                // dispatch({ type: FOUND_USER, data: response.data[0] })
            })
            .catch(function (error) {
                alert("Error in data loading");
                console.log("Error!" + error);
            });
    }


    render() {
      

        return (
            <SiteWrapper>
                <Page.Card
                    title="Dashboard"
                    RootComponent={Form}
                >

                    <Grid.Row md={12} lg={12}>
                        <Grid.Col md={12} lg={12}>
                        <iframe 
                            style={{maxWidth:'100%', height:this.state.iFrameHeight, overflow:'visible'}}
                            onLoad={() => {
                                const obj = ReactDOM.findDOMNode(this);
                                this.setState({
                                    "iFrameHeight":  1200 + 'px'
                                });
                            }} 
                            ref="iframe" 
                            src="https://slgga.codemilllabs.com/analytics/goto/c78b8a14ef91ceee52061b1778b0c50d?embed=true" 
                            width="100%" 
                            height={this.state.iFrameHeight} 
                            scrolling="no" 
                            frameBorder="0"
                        />
                        {/* <iframe marginheight="0" frameborder="0" width="100%" height="300%" src="https://slgga.codemilllabs.com/analytics/goto/c78b8a14ef91ceee52061b1778b0c50d?embed=true"></iframe>
                         */}
                         </Grid.Col>
                    </Grid.Row>
                </Page.Card>
            </SiteWrapper>
        );
    }
}


export default CustomerDataViewPage;
