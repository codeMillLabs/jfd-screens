import React, { Component } from 'react';
import axios from 'axios';

import {
    Page,
    Grid,
    Form,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";
import 'react-table/react-table.css'
import '../App.css';


class CustomerDataViewPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchText: '',

        }

        this.searchData = this.searchData.bind(this);
        this.onLoad = this.onLoad.bind(this);
    }

    onLoad = () => {
        const value = {
            memberId: '',
            customerName: '',
            deceasedName: '',
            telNumber: '',
            email: '',
            address1: '',
            address2: '',
            city: '',
            svc_jfd: '',
            svc_vip: '',
            svc_parlour: '',
            desc_1: '',
            formErrors: {}
        };
        this.setState(value);
    }


    searchData = ({ field, value }) => {

        if (value.lenght < 3) {
            return false;
        }

        this.setState({ [field]: value });

        const searchQuery = {
            searchText: this.state.searchText
        };

        console.log(searchQuery);


        axios.post(`/api/customer/search`, searchQuery)
            .then(function (response) {

                // dispatch({ type: FOUND_USER, data: response.data[0] })
            })
            .catch(function (error) {
                alert("Error in data loading");
                console.log("Error!" + error);
            });
    }


    render() {
      

        return (
            <SiteWrapper>
                <Page.Card
                    title="Dashboard"
                    RootComponent={Form}
                >

                    <Grid.Row>
                        <Grid.Col md={12} lg={12}>
                        <iframe src="https://slgga.codemilllabs.com/analytics/goto/c78b8a14ef91ceee52061b1778b0c50d?embed=true" height="600" width="800"></iframe>
                        </Grid.Col>
                    </Grid.Row>
                </Page.Card>
            </SiteWrapper>
        );
    }
}


export default CustomerDataViewPage;
