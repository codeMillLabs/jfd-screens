import React, { Component } from 'react';
import axios from 'axios';

import {
    Page,
    Grid,
    Card,
    Button,
    Form,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";
import '../App.css';


const required = fieldValue => fieldValue ? undefined : "Please enter a valid value";

const decimalValidation = fieldValue => {
   var decimalValid = fieldValue.match(/^\d+(?:\.\d{0,2})$/i);
   return decimalValid ? undefined : "Please enter a correct decimal value"
}

const emailValidation = fieldValue => {
    var emailValid = fieldValue.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    return emailValid ? undefined : "Please enter a valid email"
}

class CustomerDataFormPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            memberId: '',
            cusFirstName: '',
            cusLastName: '',
            deceasedName: '',
            telNumber: '',
            email: '',
            address1: '',
            address2: '',
            city: '',
            casket: '',
            invoiceAmount: '',
            plr_name: '',
            svc_jfd: '',
            svc_vip: '',
            svc_parlour: '',
            svc_respect: '',
            desc_1: '',
            validations: {
                cusFirstName: [required],
                cusLastName: [required],
                deceasedName: [required],
                telNumber: [required],
                email: [required, emailValidation],
                address1: [required],
                city: [required],
                invoiceAmount: [required, decimalValidation],
                casket: [required]
            },
            formErrors: {}
        }

        this.saveForm = this.saveForm.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
        this.validateFields = this.validateFields.bind(this);

    }

    onCancel = () => {
        const value = {
            id: '',
            memberId: '',
            cusFirstName: '',
            cusLastName: '',
            deceasedName: '',
            telNumber: '',
            email: '',
            address1: '',
            address2: '',
            city: '',
            casket: '',
            invoiceAmount: 0,
            plr_name: '',
            svc_jfd: '',
            svc_vip: '',
            svc_respect: '',
            svc_parlour: '',
            desc_1: '',
            formErrors: {}
        };
        this.setState(value);
    }

    handleValueChange = ({ field, value }) => {
        this.setState({ [field]: value });
    }


    validateFields = () => {
        let fields = ["cusFirstName", "cusLastName", "deceasedName", "telNumber", "email", "address1", "city", 
        "casket", "invoiceAmount"];

        var errors = {
            hasError: false
        }

        fields.forEach((fieldName) => {
            this.state.validations[fieldName].forEach((validation) => {
                if (typeof validation === "function") {
                    var errorMessage = validation(this.state[fieldName]);

                    if (errorMessage) {
                        errors.hasError = true;
                        errors[fieldName] = errorMessage;
                        console.log(fieldName + ':' + errorMessage)

                        return;
                    }

                } else {
                    //this.validateField(validation, this.state.form[validation]);
                }
            });
        })

        this.setState({ formErrors: errors }, function () {
            console.log(this.state.formErrors);
        });

        return errors;
    }



    saveForm = (e) => {
        e.preventDefault();

        let errors = this.validateFields();
        console.log(errors.hasError)
        if (errors.hasError) {
            console.log("Validation errors");

            //alert("Please fill all the required fields");
            return false;
        }

        const id = (this.state.id == '') ? new Date().getTime(): this.state.id;

        const customerRegData = {
            id: id,
            memberId: this.state.memberId,
            cusFirstName: this.state.cusFirstName,
            cusLastName: this.state.cusLastName,
            deceasedName: this.state.deceasedName,
            telNumber: this.state.telNumber,
            email: this.state.email,
            address1: this.state.address1,
            address2: this.state.address2,
            city: this.state.city,
            casket: this.state.casket,
            invoiceAmount: parseFloat(this.state.invoiceAmount),
            plr_name: this.state.plr_name,
            svc_jfd: this.state.svc_jfd,
            svc_vip: this.state.svc_vip,
            svc_respect: this.state.svc_respect,
            svc_parlour: this.state.svc_parlour,
            desc_1: this.state.desc_1
        };

        console.log(customerRegData)

        axios.post(`/api/customer`, customerRegData)
            .then(function (response) {
                alert("saved successfully");
            })
            .catch(function (error) {
                alert("saved successfully");
                console.log("Error!" + error);
            });
    }


    render() {

        return (
            <SiteWrapper>
                <Page.Card
                    title="Customer Information"
                    RootComponent={Form}
                    footer={
                        <Card.Footer>
                            <div className="d-flex">
                                <Button link onClick={() => this.saveForm} >Cancel</Button>
                                <Button type="submit" color="primary" className="ml-auto"
                                    onClick={(e) => this.saveForm(e)}>
                                    Save
                                </Button>
                            </div>
                        </Card.Footer>
                    }
                >

                    <Grid.Row>
                        <Grid.Col md={12} lg={12}>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Member Id">
                                        <Form.Input name="memberId" placeholder="Member Id..."
                                            onChange={(e) => this.handleValueChange({ field: 'memberId', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="First Name" isRequired>
                                        <Form.Input name="cusFirstName" placeholder="First Name..."
                                            error={this.state.formErrors['cusFirstName']}
                                            onChange={(e) => this.handleValueChange({ field: 'cusFirstName', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Last Name" isRequired>
                                        <Form.Input name="cusLastName" placeholder="Last Name..."
                                            error={this.state.formErrors['cusLastName']}
                                            onChange={(e) => this.handleValueChange({ field: 'cusLastName', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <Form.Group label="Name of deceased" isRequired>
                                <Form.Input name="deceasedName" placeholder="Deceased Name..."
                                    error={this.state.formErrors['deceasedName']}
                                    onChange={(e) => this.handleValueChange({ field: 'deceasedName', value: e.target.value })} />
                            </Form.Group>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Telephone number" isRequired>
                                        <Form.Input name="telNumber" placeholder="Telephone Number..."
                                            error={this.state.formErrors['telNumber']}
                                            onChange={(e) => this.handleValueChange({ field: 'telNumber', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Email address" isRequired>
                                        <Form.Input name="email" placeholder="Email..."
                                            error={this.state.formErrors['email']}
                                            onChange={(e) => this.handleValueChange({ field: 'email', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <Form.Group label="Address" isRequired>
                                <Form.Input className="mb-5" name="address1"
                                    error={this.state.formErrors['address1']}
                                    placeholder="Address line 1..." onChange={(e) => this.handleValueChange({ field: 'address1', value: e.target.value })} />
                                <Form.Input name="address2" placeholder="Address line 2..." onChange={(e) => this.handleValueChange({ field: 'address2', value: e.target.value })} />
                            </Form.Group>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="City" isRequired>
                                        <Form.Input name="city" placeholder="City..."
                                            error={this.state.formErrors['city']}
                                            onChange={(e) => this.handleValueChange({ field: 'city', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>

                                </Grid.Col>
                            </Grid.Row>
                            <hr />
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Type of Casket" isRequired>
                                        <Form.Input name="casket" placeholder="casket..."
                                            error={this.state.formErrors['casket']}
                                            onChange={(e) => this.handleValueChange({ field: 'casket', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                                <Grid.Col md={6} lg={6}>
                                    <Form.Group label="Invoice Amount" isRequired>
                                        <Form.Input name="invoiceAmount" placeholder="Total invoice amount"
                                            error={this.state.formErrors['invoiceAmount']}
                                            onChange={(e) => this.handleValueChange({ field: 'invoiceAmount', value: e.target.value })} />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <hr />
                            <Grid.Row>
                                <Grid.Col md={6} lg={6}>
                                <Form.Group label="Parlour">
                                        <Form.Radio
                                            name="plr_name"
                                            label="Parlour Funeral"
                                            value="Parlour Funeral"
                                            onChange={(e) => this.handleValueChange({ field: 'plr_name', value: e.target.value })}
                                        />
                                        <Form.Radio
                                            name="plr_name"
                                            label="Home Funeral"
                                            value="Home Funeral"
                                            onChange={(e) => this.handleValueChange({ field: 'plr_name', value: e.target.value })}
                                        />
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>
                            <hr />
                            <Grid.Row>
                                <Grid.Col md={12} lg={12}>
                                    <Form.Group label="Services">
                                        <Form.Checkbox
                                            name="svc_jfd"
                                            label="Jayaratne Funeral Directors"
                                            value="Jayaratne Funeral Directors"
                                            onChange={(e) => this.handleValueChange({ field: 'svc_jfd', value: e.target.value })}
                                        />
                                        <Form.Checkbox
                                            name="svc_vip"
                                            label="VIP"
                                            value="VIP"
                                            onChange={(e) => this.handleValueChange({ field: 'svc_vip', value: e.target.value })}
                                        />
                                        <Form.Checkbox
                                            name="svc_respect"
                                            label="Respect"
                                            value="Respect"
                                            onChange={(e) => this.handleValueChange({ field: 'svc_respect', value: e.target.value })}
                                        />
                                    </Form.Group>
                                    <Form.Group
                                        label={<Form.Label aside="" children="Other details" />}>
                                        <Form.Textarea
                                            name="desc_1"
                                            rows={3}
                                            placeholder="Other description..."
                                            defaultValue=""
                                            onChange={(e) => this.handleValueChange({ field: 'desc_1', value: e.target.value })}
                                        />
                                    </Form.Group>

                                </Grid.Col>
                            </Grid.Row>
                        </Grid.Col>
                    </Grid.Row>
                </Page.Card>
            </SiteWrapper>
        );
    }
}


export default CustomerDataFormPage;
