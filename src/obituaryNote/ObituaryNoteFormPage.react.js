import React, { Component } from 'react';
import axios from 'axios';

import {
    Page,
    Grid,
    Card,
    Button,
    Form,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";
import MagicDropzone from 'react-magic-dropzone';

import "./styles.css";
import '../App.css';


class ObituaryNoteFormPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            location: 'office_1',
            parlourNo: 'A',
            name: '',
            subTitle: '',
            images: [],
            previews: [],
            img_1: '',
            img_2: '',
            img_3: '',
            img_4: '',
            img_5: '',
            desc_1: '',
            desc_2: '',
            desc_3: '',
            msgs: ''
        }

        this.saveForm = this.saveForm.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);

    }

    onCancel = () => {
        const value = {
            location: 'office_1',
            parlourNo: 'A',
            name: '',
            subTitle: '',
            images: [],
            previews: [],
            img_1: '',
            img_2: '',
            img_3: '',
            img_4: '',
            img_5: '',
            desc_1: '',
            desc_2: '',
            desc_3: ''
        };
        this.setState(value);
    }

    handleValueChange = ({ field, value }) => {
        this.setState({ [field]: value });
        console.log("::::" + value);
    }

    saveForm = (e) => {
        e.preventDefault();
        const parlourData = {
            location: this.state.location,
            parlourNo: this.state.parlourNo,
            name: this.state.name,
            subTitle: this.state.subTitle,
            descriptions: [],
            images: this.state.images
        }

        if (this.state.desc_1) {
            parlourData.descriptions.push(this.state.desc_1);
        }
        if (this.state.desc_2) {
            parlourData.descriptions.push(this.state.desc_2);
        }
        if (this.state.desc_3) {
            parlourData.descriptions.push(this.state.desc_3);
        }

        if (this.state.img_1) {
            parlourData.images.push(this.state.img_1);
        }
        if (this.state.img_2) {
            parlourData.images.push(this.state.img_2);
        }
        if (this.state.img_3) {
            parlourData.images.push(this.state.img_3);
        }
        if (this.state.img_4) {
            parlourData.images.push(this.state.img_4);
        }
        if (this.state.img_5) {
            parlourData.images.push(this.state.img_5);
        }

        console.log(parlourData)

        axios.post(`/api/parlour/add`, parlourData)
            .then(function (response) {
                alert("saved successfully");
            })
            .catch(function (error) {
                alert("saved successfully");
                console.log("Error!" + error);
            });

    }

    onDrop = (accepted, rejected, links) => {
        console.log('acceptedFiles reading started' + accepted);
        var images = [];
        accepted = accepted.map(v => {
            var reader = new FileReader();
            reader.readAsDataURL(v);
            reader.onload = function () {
                images.push(reader.result)
            };

           return v.preview;
        });

        console.log(accepted);
        var previews = [...accepted];
        this.setState({ images: images, previews: previews});
    }

    render() {

        return (
            <SiteWrapper>
                <Page.Card
                    title="Obituary Form"
                    RootComponent={Form}
                    footer={
                        <Card.Footer>
                            <div className="d-flex">
                                <Button link onClick={() => this.saveForm} >Cancel</Button>
                                <Button type="submit" color="primary" className="ml-auto"
                                    onClick={(e) => this.saveForm(e)}>
                                    Save
                                </Button>
                            </div>
                        </Card.Footer>
                    }
                >

                    <Grid.Row>
                        <Grid.Col md={12} lg={12}>
                            <Form.Group label="Location">
                                <Form.Select className="w-auto mr-2" onChange={(e) => this.handleValueChange({ field: 'location', value: e.target.value })}>
                                    <option value="office_1">Head-Office</option>
                                    <option value="office_2">The Respect</option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group label="Parlour">
                                <Form.Select className="w-auto mr-2" onChange={(e) => this.handleValueChange({ field: 'parlourNo', value: e.target.value })}>
                                    <option value="A">Parlour-A</option>
                                    <option value="B">Parlour-B</option>
                                    <option value="C">Parlour-C</option>
                                    <option value="D">Parlour-D</option>
                                    <option value="1">New-1</option>
                                    <option value="2">New-2</option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group label="Name">
                                <Form.Input name="name" placeholder="Name..." onChange={(e) => this.handleValueChange({ field: 'name', value: e.target.value })} />
                            </Form.Group>
                            <Form.Group label="Sub-Title">
                                <Form.Input name="subTitle" placeholder="Subtitle..." onChange={(e) => this.handleValueChange({ field: 'subTitle', value: e.target.value })} />
                            </Form.Group>
                            <Grid.Row>
                                <Grid.Col md={12} lg={12}>
                                    <Form.Group
                                        label={<Form.Label aside="" children="Upload Images" />}>
                                        <MagicDropzone
                                            className="Dropzone"
                                            accept="image/jpeg, image/png, .jpg, .jpeg, .png"
                                            onDropAccepted={this.onDrop}
                                            onClick={(e) => { }}>
                                            <div className="Dropzone-content">
                                                {this.state.images.length > 0 ? (
                                                    this.state.images.map((v, i) => (
                                                        <img key={i} alt="" className="Dropzone-img" src={v} />
                                                    ))
                                                ) : (
                                                        "Drop some files on me!"
                                                    )}
                                            </div>
                                        </MagicDropzone>
                                    </Form.Group>
                                </Grid.Col>
                            </Grid.Row>

                            <Form.Group
                                label={<Form.Label aside="" children="Description Page 1" />}>
                                <Form.Textarea
                                    name="description_1"
                                    rows={3}
                                    placeholder="Content description.."
                                    defaultValue=""
                                    onChange={(e) => this.handleValueChange({ field: 'desc_1', value: e.target.value })}
                                />
                            </Form.Group>
                            <Form.Group
                                label={<Form.Label aside="" children="Description Page 2" />}>
                                <Form.Textarea
                                    name="description_2"
                                    rows={3}
                                    placeholder="Content description.."
                                    defaultValue=""
                                    onChange={(e) => this.handleValueChange({ field: 'desc_2', value: e.target.value })}
                                />
                            </Form.Group>
                            <Form.Group
                                label={<Form.Label aside="" children="Description Page 3" />}>
                                <Form.Textarea
                                    name="description_3"
                                    rows={3}
                                    placeholder="Content description.."
                                    defaultValue=""
                                    onChange={(e) => this.handleValueChange({ field: 'desc_3', value: e.target.value })}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                </Page.Card>
            </SiteWrapper>
        );
    }
}


export default ObituaryNoteFormPage;
