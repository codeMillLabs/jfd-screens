import React, { Component } from 'react';
import axios from 'axios';
import { Carousel } from 'react-responsive-carousel';

import {
    Grid,
    Card,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";
import '../App.css';
import './black_theme.css';

import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';


class ObituaryNote extends Component {

    state = {
        parlourData: {
            location: '',
            parlourNo: '',
            name: '',
            subTitle: '',
            descriptions: [""],
            images: []
        },
        descriptionView: <div></div>,
        imageView: <div></div>
    }

    openForm = () => {
        console.log("message -->");
    }

    componentDidMount() {
        var location = this.props.match.params.location;
        var parlourId = this.props.match.params.parlourId;

        axios.get(`/api/parlour/` + location + `/` + parlourId)
            .then(res => {
                console.log(res);
                const parlourData = res.data;

                const descPages = parlourData.descriptions.map(function (text, indx) {
                    return <div key={indx} className="p-5 black-bg">
                        <p className="card-display-text white-text">
                            {text}
                        </p>
                    </div>;
                });

                const descriptionView = <Carousel
                    autoPlay={true}
                    showArrows={false}
                    showThumbs={false}
                    showStatus={false}
                    infiniteLoop={true}
                    interval={6000}>
                    {descPages}
                </Carousel>

                const imagePages = parlourData.images.map(function (imgUrl, indx) {
                    return <div key={indx}>
                        <img src={imgUrl} />
                    </div>;
                });

                const imageView = <Carousel
                    autoPlay={true}
                    showArrows={false}
                    showThumbs={false}
                    showStatus={false}
                    infiniteLoop={true}>
                    {imagePages}
                </Carousel>

                this.setState({ descriptionView });
                this.setState({ imageView });
                this.setState({ parlourData });
            })
    }

    render() {

        return (
            <SiteWrapper headerImage="/demo/brand/respect-logo.JPG">
                <Grid.Row cards sm={12} md={12} lg={12} className="black-bg">
                    <Grid.Col sm={8} md={8} lg={8}>
                        <div className="text-wrap p-5 black-bg">
                            <div className="card-name-text white-text">{this.state.parlourData.name}</div>
                            <div className="card-sub-text white-text">
                                {this.state.parlourData.subTitle}
                            </div>
                            <div>
                                {this.state.descriptionView}
                            </div>
                        </div>
                    </Grid.Col>
                    <Grid.Col>
                        <Card className="black-bg">
                            {this.state.imageView}
                        </Card>
                    </Grid.Col>
                </Grid.Row>
            </SiteWrapper >
        );
    }
}


export default ObituaryNote;
