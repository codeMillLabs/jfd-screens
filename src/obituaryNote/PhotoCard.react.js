
import * as React from "react";

import {
    Icon,
    Card,
    Text,
} from "tabler-react";

type Props = {|
  +children?: React.Node,
  +title?: string,
  +subtitle?: string,
  +imgUrl?: string,
  +imgAlt?: string,
|};

function PhotoCard({
  children,
  title,
  subtitle,
  price,
  imgUrl,
  imgAlt,
}: Props): React.Node {
  return (
    <Card>
      <Card.Body>
        <div className="mb-4 text-center">
          <img src={imgUrl} alt={imgAlt} />
        </div>
        <Card.Title>{title}</Card.Title>
        <Text className="text-center">{subtitle}</Text>

        <div className="mt-5 d-flex align-items-center">
          <div className="product-price">
             <br/>
             <br/>
             <br/>
             <br/>
             <br/>
             <br/>
             <br/>
             <br/>
            {/* <strong>{price}</strong> */}
          </div>
          
        </div>
      </Card.Body>
    </Card>
  );
}

export default PhotoCard;
