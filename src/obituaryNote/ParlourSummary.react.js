import React, { Component } from 'react';
import axios from 'axios';

import {
    Grid,
    Card,
    Table,
} from "tabler-react";

import SiteWrapper from "../SiteWrapper.react";

import '../App.css';
import './black_theme.css';



class ParlourSummary extends Component {

    state = {
        summaryData: []
    }

    openForm = () => {
        console.log("message -->");
    }

    componentDidMount() {
        var location = this.props.match.params.location;
        axios.get(`/api/parlour/summary/` + location)
            .then(res => {
                console.log(res);
                const parlourData = res.data;
                console.log(parlourData);
                const rows = [];

                parlourData.map((d, index) => {

                    if (d.name) {
                        rows.push(<Table.Row key={index}>
                            <Table.Col>
                                <img
                                    alt=""
                                    src={d.images[0]}
                                    className="preview-img"
                                />
                            </Table.Col>
                            <Table.Col className="card-name-text white-text">
                                {d.name}
                            </Table.Col>
                            <Table.Col className="text-right text-muted d-none d-md-table-cell text-nowrap card-display-text white-text">
                                Parlour : {d.parlourNo}
                            </Table.Col>
                        </Table.Row>)
                    }
                });

                this.setState({ summaryData: rows });
            })
    }

    render() {

        return (
            <SiteWrapper>
                <Grid.Row cards sm={12} md={12} lg={12}>
                    <Grid.Col >
                        <Card className="black-bg">
                            <Table className="card-table table-vcenter">
                                <Table.Body>
                                    {this.state.summaryData}
                                </Table.Body>
                            </Table>
                        </Card>
                    </Grid.Col>
                </Grid.Row>
            </SiteWrapper >
        );
    }
}


export default ParlourSummary;
