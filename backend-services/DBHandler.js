var mysql = require('mysql');

var pool = mysql.createPool({
    connectionLimit: 10,
    host: "148.72.232.169",
    user: "jfd_db_user",
    password: "sRvu^965",
    database: "jfd_data_repository"
});


var dbHandler = (function () {

    function _query(query, params, callback) {

        pool.getConnection(function (err, connection) {
            if (err) {
                connection.release();
                callback(null, err);
                throw err;
            }

            connection.query(query, params, function (err, rows) {
                connection.release();
                if (!err) {
                    callback(rows);
                }
                else {
                    callback(null, err);
                }

            });

            connection.on('error', function (err) {
                connection.release();
                // callback(null, err);
                //throw err;
            });
        });
    };

    return {
        query: _query
    };
})();


exports.dbHandler = dbHandler;