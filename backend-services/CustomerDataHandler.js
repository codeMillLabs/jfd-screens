
var dbHandler = require('./DBHandler').dbHandler;

var insertQuery = "INSERT INTO `jfd_customer_data`( `id`, `memberId`, `cusFirstName`, " +
    "`cusLastName`, `deceasedName`, `telNumber`, `email`, `address1`, `address2`, `city`, " +
    "`casket`,  `invoiceAmount`, `plr_name`, `svc_jfd`, `svc_vip`, " +
    " `svc_respect`, `svc_parlour`, `desc_1`) " +
    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

var customerDBHandler = (function () {

    function _insert(customerData, callback) {
        var params = [];
        console.log(customerData);
        for (var prop in customerData) {
            params.push(customerData[prop]);
        }
        dbHandler.query(insertQuery, params, callback);
    };

    return {
        insert: _insert
    };
})();

exports.customerDBHandler = customerDBHandler;