const express = require('express');
const bodyParser = require('body-parser');
const elasticsearch = require('elasticsearch');
const jsonfile = require('jsonfile');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const customerDBHandler = require('./backend-services/CustomerDataHandler').customerDBHandler;

const app = express();
const port = process.env.PORT || 8201;

var client = new elasticsearch.Client({
  host: 'https://slgga.codemilllabs.com/es',
  log: 'info'
});

const filepath = path.join(process.cwd(), 'datastore/');

app.use(bodyParser.json({limit: '50mb', type: 'application/json'})); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static(path.join(process.cwd(), 'build')));
app.use(cors());

app.post('/api/parlour/add', function (req, res) {
  //console.log(req.body)
  var parlourData = req.body;

  createDataStore(filepath);

  var hasError = false;
  var file = filepath + "parlour_" + parlourData.location + "_" + parlourData.parlourNo + ".json";
  jsonfile.writeFile(file, parlourData, function (err) {
    res.status(500).send(err);
    hasError = true;
  })
  if (hasError) {
    res.status(200).write(parlourData.toString).end();
  }

});

app.get('/api/parlour/summary/:location', (req, res) => {
  var location = req.params.location;
  const data = [];
  var completed = false;

  readFiles(filepath)
    .then(files => {
      console.log("loaded ", files.length);
      files.forEach((item, index) => {
        console.log("item", index, "size ", item.contents.length + "-" + item.contents.location);
        var content = JSON.parse(item.contents);
        console.log(content.location);
        if (content.location == location) {
          data.push(content);
        }
      });
      res.send(data);
    })
    .catch(error => {
      console.log(error);
    });
  //console.log(data.length);
});

app.get('/api/parlour/:location/:id', (req, res) => {
  var location = req.params.location;
  var id = req.params.id;
  var file = filepath + "parlour_" + location + "_" + id + ".json";
  jsonfile.readFile(file, function (err, data) {
    res.send(data)
  })
});

app.get('/api/parlour/:location/:id', (req, res) => {
  var location = req.params.location;
  var id = req.params.id;
  var file = filepath + "parlour_" + location + "_" + id + ".json";
  jsonfile.readFile(file, function (err, data) {
    res.send(data)
  })
});


app.post('/api/customer', function (req, res) {
  var customerData = req.body;

  customerDBHandler.insert(customerData, function (data, error) {

    if (error) {
      res.status(500).send(error);
      console.log("Error" + error);
    } else {

      customerData.createdTimestamp = customerData.id;

      const response = client.index({
        index: 'jfd-customers',
        type: '_doc',
        id: customerData.id,
        body: customerData
      });

      console.log("ES Response: " + response);

      if(response) {
        res.status(200).end();
      }
    }
  })

});

// Handle React routing, return all requests to React app
app.get('*', function (req, res) {
  res.sendFile(path.join(process.cwd(), 'build', 'index.html'));
});


function createDataStore(dir) {
  //console.log(dir)
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

function promiseAllP(items, block) {
  var promises = [];
  items.forEach(function (item, index) {
    promises.push(function (item, i) {
      return new Promise(function (resolve, reject) {
        return block.apply(this, [item, index, resolve, reject]);
      });
    }(item, index))
  });
  return Promise.all(promises);
} //promiseAll

function readFiles(dirname) {
  return new Promise((resolve, reject) => {
    fs.readdir(dirname, function (err, filenames) {
      if (err) return reject(err);
      promiseAllP(filenames,
        (filename, index, resolve, reject) => {
          fs.readFile(dirname + filename, 'utf-8', function (err, content) {
            if (err) return reject(err);
            return resolve({ filename: filename, contents: content });
          });
        })
        .then(results => {
          return resolve(results);
        })
        .catch(error => {
          return reject(error);
        });
    });
  });
}


app.listen(port, () => console.log(`Listening on port ${port}`));